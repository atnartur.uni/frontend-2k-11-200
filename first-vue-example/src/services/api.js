import { API_URL } from "@/consts";

export async function getApiStatus() {
    const response = await fetch(`${API_URL}`);
    return await response.json();
}