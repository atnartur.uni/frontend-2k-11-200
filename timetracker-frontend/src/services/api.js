import axios from 'axios';
import { BASE_URL } from '../consts.js';
import {useUserStore} from '@/store/user';

const instance = axios.create({
    baseURL: BASE_URL,
});

instance.interceptors.request.use(function (config) {
    const userState = useUserStore();
    if (userState.token) {
        config.headers.Authorization = `Token ${userState.token}`;
    }
    return config;
});

export async function login(username, password) {
    const response = await instance.post('/token/', { username: username, password: password });
    return response.data;
}

export async function getUserInfo() {
    const response = await instance.get('/user/');
    return response.data;
}

export function serializeTimeslotFilters(filters) {
    const params = {...filters};
    if (params.is_realtime === true) {
        params.is_realtime = '1';
    }
    else if (params.is_realtime === false) {
        params.is_realtime = '0';
    }
    return params;
}

export function deserializeTimeslotFilters(filters) {
    const params = {...filters};
    if (params.is_realtime === '1') {
        params.is_realtime = true;
    }
    else if (params.is_realtime === '0') {
        params.is_realtime = false;
    }
    else if (params.is_realtime !== undefined) {
        params.is_realtime = null;
    }
    return params;
}

export async function getTimeslots(filters) {
    const response = await instance.get('/timeslots/', {params: filters});
    return response.data;
}

export async function getTimeslot(id) {
    const response = await instance.get(`/timeslots/${id}`);
    return response.data;
}