/**
 * router/index.ts
 *
 * Automatic routes for `./src/pages/*.vue`
 */

// Composables
import { createRouter, createWebHistory } from 'vue-router/auto'
import { useUserStore } from '@/store/user';
import { PAGES_WITHOUT_AUTH } from '@/consts';

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
});

router.beforeEach(async (to, from) => {
  const user = useUserStore();
  if (!user.info && user.token) {
    await user.load();
  }
  if (user.isAuthorized && PAGES_WITHOUT_AUTH.includes(to.path)) {
    return {path: '/profile'};
  }
  if (!user.isAuthorized && !PAGES_WITHOUT_AUTH.includes(to.path)) {
    return {path: '/'};
  }
})

export default router
