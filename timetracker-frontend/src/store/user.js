// Utilities
import { defineStore } from 'pinia'
import { login, getUserInfo } from '@/services/api';
import { getToken, setToken, clearToken } from '@/services/localStore';

export const useUserStore = defineStore('user', {
  state: () => ({
    isLoading: false,
    error: null,
    token: getToken(),
    info: null
  }),
  actions: {
    async login(username, password) {
      if (this.isLoading) {
        return;
      }
      this.isLoading = true;
      this.error = null;
      try {
        const result = await login(username, password);
        this.token = result.token;
        setToken(result.token);
      }
      catch (e) {
        console.error(e);
        this.error = e.response.data.non_field_errors[0];
      }
      this.isLoading = false;
      return !!this.token;
    },
    async load() {
      this.isLoading = true;
      try {
        this.info = await getUserInfo();
      }
      catch (e) {
        console.error(e);
        clearToken();
        this.token = null;
      }
      this.isLoading = false;
    },
    logout() {
      this.info = null;
      this.token = null;
      clearToken();
    }
  },
  getters: {
    isAuthorized() {
      return this.info !== null;
    }
  }
})
