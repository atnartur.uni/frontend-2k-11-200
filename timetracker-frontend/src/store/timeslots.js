// Utilities
import { defineStore } from 'pinia'
import { getTimeslots } from '@/services/api';

export const useTimeslotsStore = defineStore('timeslots', {
  state: () => ({
    isLoading: false,
    error: null,
    results: [],
    filters: {
      start_date: null,
      end_date: null,
      is_realtime: null
    }
  }),
  actions: {
    async load() {
      this.isLoading = true;
      this.error = null
      console.log('start request with', this.filters);
      try {
        const data = await getTimeslots(this.filters);
        this.results = data.results;
      }
      catch (e) {
        console.error(e);
        this.error = 'Произошла ошибка с загрузкой таймслотов, попробуйте еще раз';
      }
      this.isLoading = false;
    },
    setFilters(key, value) {
      this.filters[key] = value;
    },
    setFiltersObject(filters) {
      this.filters = filters;
    }
  }
})
